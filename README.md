gitlab-runner-docker
=========

A role to set up a gitlab runner using docker.

See the official [docs](https://docs.gitlab.com/runner/install/linux-repository.html) for more details.

Note: The role works, but due to how the config file works, updating will create extra runners, and e.g. the concurrency is not applied correctly. It must be changed manually in `/etc/gitlab-runner/config.toml`. See e.g. [this issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1539), [this issue](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/1697) or [this issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3553)

It seems that the correct way is to go through the API, e.g. using the [ansible module](https://docs.ansible.com/ansible/latest/collections/community/general/gitlab_runner_module.html) for it.


Requirements
------------

None

Role Variables
--------------

see [defauls/main.yml](defauls/main.yml)

Dependencies
------------

None

Example Playbook
----------------

See [test.yml](defauls/main.yml)

License
-------

BSD

Author Information
------------------

Moozer

* Twitter: @bigmoozer
* web: https://moozer.gitlab.io/

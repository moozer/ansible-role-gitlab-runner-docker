tests
=======


1. Create the file `host_vars/default.yml` with the line `glrun_registration_token: <your token>`
2. Use `vagrant up` to spin up a runner locally.

This will spin up the runner and register it with gitlab.com.
See the [roles/gitlab-runner-docker/vars/defaults.yml](defaults.yml) from the role for more options.

